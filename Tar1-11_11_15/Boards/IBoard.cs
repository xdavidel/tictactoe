﻿

namespace Boards
{
    interface IBoard
    {
		int Size { get; }

		char this[int row,int col]
		{
			get;
		}  

		int EmptySpots { get; }

		void Mark(int row, int col, char marking);

    }
}
