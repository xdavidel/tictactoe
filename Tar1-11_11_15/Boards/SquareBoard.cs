﻿using System;

using Factories;
using System.Collections.Generic;
using System.Collections;

namespace Boards
{
    [RegisterInFactory(typeof(ISquareBoard))]
    class SquareBoard:ISquareBoard,IEnumerable<char>
    {
		char[,] _matrix;
		int _size;
		int _emptySpots;

		public IEnumerable<char> ColumnScanner
		{
			get
			{
				return new ColumnScanEnumerable(this);
			}
		}

		public IEnumerable<char> DiagonalDownScanner
		{
			get
			{
				return new DiagonalDownScanEnumerable(this);
			}
		}

		public IEnumerable<char> DiagonalUpScanner
		{
			get
			{
				return new DiagonalUpScanEnumerable(this);
			}
		}

		public int EmptySpots
		{
			get
			{
				return _emptySpots;
			}
		}

		public int Size	{ get{return _size; } }

		public char this[int row,int col]
		{
			get
			{
				if (row >= _size || col >= _size)
					throw new OutOfBoardRangeException(row, col);
				return _matrix[row, col];
			}
		}

		public SquareBoard()
		{
			_size = 3;
			_emptySpots = _size * _size;

			_matrix = new char[_size, _size];
			for (int i = 0; i < _size; i++)
				for (int j = 0; j < _size; j++)
					_matrix[i, j] = '_';
		}

		public SquareBoard(int size)
		{
			_size = size;
			_emptySpots = size * size;

			_matrix = new char[size,size];
			for (int i = 0; i < size; i++)
				for (int j = 0; j < size; j++)
					_matrix[i, j] = '_';
		}
		

		public void Mark(int row, int col, char marking)
		{
			if (_emptySpots == 0)
				throw new NoSpotsAvailableException("No more available spots!");

			if (_matrix[row, col] != '_')
				throw new SpotTakenException(row,col);

			if (row >= _size || col >= _size)
				throw new OutOfBoardRangeException(row, col);

			_matrix[row, col] = marking;
			_emptySpots--;
		}

		public void CopyTo(ref char[,] arr)
		{
			for (int i = 0; i < _size; i++)
			{
				for (int j = 0; j < _size; j++)
				{
					arr[i, j] = _matrix[i, j];
				}
			}
		}

		public void CopyFrom(char[,] arr)
		{
			int count = 0;

			for (int i = 0; i < _size; i++)
			{
				for (int j = 0; j < _size; j++)
				{
					_matrix[i, j] = arr[i, j];
					if (arr[i, j] == '_')
						count++;
				}
			}
			_emptySpots = count;
		}


		#region Enumerations


		public IEnumerator<char> GetEnumerator()
		{
			return new RowScanEnumerator(this);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			throw new NotImplementedException();
		}

		private abstract class BoardEnumerable : IEnumerable<char>
		{
			protected IBoard _board;

			public BoardEnumerable(IBoard board)
			{
				_board = board;
			}

			public abstract IEnumerator<char> GetEnumerator();

			IEnumerator IEnumerable.GetEnumerator()
			{
				return null;
			}
		}


		private abstract class BoardEnumerator : IEnumerator<char>
		{
			protected IBoard _board;
			protected int _row;
			protected int _col;


			public BoardEnumerator(IBoard board)
			{
				_board = board;
				Reset();
			}

			public char Current
			{
				get
				{
					return _board[_row, _col];
				}
			}

			object IEnumerator.Current
			{
				get
				{
					return _board[_row, _col];
				}
			}

			public void Dispose()
			{
				
			}

			public abstract void Reset();

			public abstract bool MoveNext();
		}


		private class RowScanEnumerator : BoardEnumerator
		{
			public RowScanEnumerator(IBoard board) : base(board) { }

			public override bool MoveNext()
			{
				bool retVal = false;

				if (_col < _board.Size - 1)
				{
					_col++;
					retVal = true;
				}
				else if (_row < _board.Size - 1)
				{
					_col = 0;
					_row++;
					retVal = true;
				}

				return retVal;
			}

			public override void Reset()
			{
				_row = 0;
				_col = -1;
			}
		}

		private class ColumnScanEnumerable : BoardEnumerable
		{
			public ColumnScanEnumerable(IBoard board) : base(board){ }

			public override IEnumerator<char> GetEnumerator()
			{
				return new ColumnScanEnumerator(_board);
            }

		}

		private class ColumnScanEnumerator : BoardEnumerator
		{
			public ColumnScanEnumerator(IBoard board) : base(board) { }

			public override bool MoveNext()
			{
				bool retVal = false;

				if (_row < _board.Size - 1)
				{
					_row++;
					retVal = true;
				}
				else if (_col < _board.Size - 1)
				{
					_row = 0;
					_col++;
					retVal = true;
				}

				return retVal;
			}

			public override void Reset()
			{
				_row = -1;
				_col = 0;
			}
		}

		private class DiagonalDownScanEnumerable : BoardEnumerable
		{
			public DiagonalDownScanEnumerable(IBoard board) : base(board) { }

			public override IEnumerator<char> GetEnumerator()
			{
				return new DiagonalDownScanEnumerator(_board);
			}

		}

		private class DiagonalDownScanEnumerator : BoardEnumerator
		{
			public DiagonalDownScanEnumerator(IBoard board) : base(board) { }

			public override bool MoveNext()
			{


				bool retVal = false;

				if (_row < _board.Size - 1 && _col < _board.Size - 1)
				{
					_row++;
					_col++;
					retVal = true;
				}

				return retVal;
			}

			public override void Reset()
			{
				_row = -1;
				_col = -1;
			}

		}

		private class DiagonalUpScanEnumerable : BoardEnumerable
		{
			public DiagonalUpScanEnumerable(IBoard board) : base(board) { }

			public override IEnumerator<char> GetEnumerator()
			{
				return new DiagonalUpScanEnumerator(_board);
			}

		}

		private class DiagonalUpScanEnumerator : BoardEnumerator
		{
			public DiagonalUpScanEnumerator(IBoard board) : base(board) { }

			public override bool MoveNext()
			{
				bool retVal = false;

				if (_row > 0 && _col < _board.Size - 1)
				{
					_row--;
					_col++;
					retVal = true;
				}

				return retVal;
			}

			public override void Reset()
			{
				_row = 3;
				_col = -1;
			}

		}



		#endregion

		#region Exceptions

		private abstract class BoardIndexerException:Exception
		{
			public int Row { get; private set; }
			public int Col { get; private set; }

			public BoardIndexerException(int row, int col)
			{
				Row = row;
				Col = col;

			}
		}


		private class SpotTakenException: BoardIndexerException
		{
			public override string Message
			{
				get
				{
					return string.Format("Spot {0},{1} is taken!!!", Row, Col);
                }
			}

			public SpotTakenException(int row,int col):base(row,col)	{}
		}

		private class NoSpotsAvailableException : Exception
		{
			public NoSpotsAvailableException(string msg) : base(msg)
			{
			}
		}

		private class OutOfBoardRangeException : BoardIndexerException
		{
			public override string Message
			{
				get
				{
					return string.Format("Spot {0},{1} is out of range!", Row, Col);
                }
			}

			public OutOfBoardRangeException(int row, int col) : base(row, col) { }
		}


		#endregion

	}
}
