﻿using Boards;
using Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tar1_11_11_15
{
	class ConsoleMoveReader
	{
		public ConsoleMoveReader(HumanPlayer player)
		{
			player.OnMoveRead += ReadMove;
		}

		int[] ReadMove(IBoard board)
		{
			int[] retVal = new int[2];
			int row;
			int col;

			bool validInput;

			while (true)
			{
				Console.Write("Select row: ");
				validInput = int.TryParse(Console.ReadLine(), out row);
				Console.Write("Select column: ");
				validInput &= int.TryParse(Console.ReadLine(), out col);

				if (validInput)
				{
					if (row >= board.Size || col >= board.Size)
						Console.WriteLine("Index {0},{1} is out of range", row, col);

					else if (board[row, col] == '_')
						break;

					else Console.WriteLine("invalid spot!!");
				}

			}

			retVal[0] = row;
			retVal[1] = col;

			return retVal;
		}
	}
}
