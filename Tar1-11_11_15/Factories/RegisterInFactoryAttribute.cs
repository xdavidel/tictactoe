﻿using System;

namespace Factories
{
	[System.AttributeUsage(AttributeTargets.Class)]
	class RegisterInFactoryAttribute:Attribute
	{
		protected Type _interfaceType;

		public Type InterfaceType
		{
			get { return _interfaceType; }

		}
		public RegisterInFactoryAttribute(Type interfaceType)
		{
			_interfaceType = interfaceType;
		}
	}
}
