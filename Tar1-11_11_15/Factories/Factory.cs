﻿using System;
using System.Collections.Generic;
using System.Reflection;

using Players;

namespace Factories
{
	/// <summary>
	/// Singelton Factory Class
	/// </summary>
    class Factory
    {
		protected static Factory _instance = null;

		public static Factory Instance
		{
			get
			{
				if (_instance == null)
					_instance = new Factory();
				return _instance;
			}
		}

		Dictionary<string, Type> _dictionary;

		private Factory()
        {
			_dictionary = new Dictionary<string, Type>();

			Assembly assembly = Assembly.GetExecutingAssembly();
            foreach (Type t in assembly.GetTypes())
            {
				RegisterInFactoryAttribute attr = t.GetCustomAttribute<RegisterInFactoryAttribute>();
				if (attr != null)
                {
                    RegisterType(attr.InterfaceType, t);
                }
			}
        }

		/// <summary>
		/// Create any object via Factory
		/// </summary>
		/// <param name="type">The inteface of the type</param>
		/// <returns>Object of the required type</returns>
		public object CreateObject(Type type)
		{
			Type mType = _dictionary[type.Name];
			return Activator.CreateInstance(mType);
		}

		/// <summary>
		/// Register Classes into factory
		/// </summary>
		/// <param name="InterfaceType">The inteface key</param>
		/// <param name="type">The type to register</param>
		public void RegisterType(Type InterfaceType, Type type)
		{
			_dictionary.Add(InterfaceType.Name, type);
		}

		/// <summary>
		/// Add decoration to player
		/// </summary>
		/// <param name="InterfaceType">The inteface key</param>
		/// <param name="xoPlayer">The player to add decoration to</param>
		public void AddPlayerDecoration(Type InterfaceType, ref IXOPlayer xoPlayer)
		{
			Type mType = _dictionary[InterfaceType.Name];
			xoPlayer = (IXOPlayer)Activator.CreateInstance(mType, xoPlayer);
		}


	}
}