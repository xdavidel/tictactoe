﻿using System;

using Players;
using Boards;

namespace Decorators
{
	abstract class XOPlayerDecorator: IXOPlayer
	{
		protected IXOPlayer _xoPlayer;

		public char Marking
		{
			get
			{
				return _xoPlayer.Marking;
			}

			set
			{
				_xoPlayer.Marking = value;
			}
		}

		public XOPlayerDecorator(IXOPlayer xoPlayer)
		{
			_xoPlayer = xoPlayer;
		}

		public abstract void MakeMove(IBoard board);
	}
}

