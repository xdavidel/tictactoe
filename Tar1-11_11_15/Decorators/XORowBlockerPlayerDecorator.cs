﻿using System;

using Players;
using Boards;
using Factories;

namespace Decorators
{
	[RegisterInFactory(typeof(IRowBlockerPlayerDecorator))]
	class XORowBlockerPlayerDecorator : XOPlayerDecorator, IComputerPlayer,IDiagonalBlockerDecorator
	{

		public XORowBlockerPlayerDecorator(IXOPlayer xoPlayer):base(xoPlayer)
		{

		}

		/// <summary>
		/// Find the first empty spot unless a row is almost full
		/// </summary>
		/// <param name="board">the board to mark</param>
		public override void MakeMove(IBoard board)
		{
			int count = 0, blockingPos = -1;
			

			for (int row = 0; row < board.Size; row++)
			{
				for (int col = 0; col < board.Size; col++)
				{
					if (board[row, col] != Marking && board[row, col] != '_')
						count++;

					if (board[row, col] == '_')
						blockingPos = col;

					if (count == board.Size - 1 && blockingPos != -1)
					{
						board.Mark(row, blockingPos,Marking);
						return;
					}
				}
				count = 0;
				blockingPos = -1;
			}

            _xoPlayer.MakeMove(board);
		}
	}
}
