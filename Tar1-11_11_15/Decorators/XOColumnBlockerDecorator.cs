﻿using System;

using Players;
using Boards;
using Factories;

namespace Decorators
{
	[RegisterInFactory(typeof(IColumnBlockerDecorator))]
	class XOColumnBlockerDecorator:XOPlayerDecorator, IComputerPlayer,IColumnBlockerDecorator
	{
		public XOColumnBlockerDecorator(IXOPlayer xoPlayer):base(xoPlayer)
		{

		}


		public override void MakeMove(IBoard board)
		{
			int count = 0, blockingPos = -1;


			for (int col = 0; col < board.Size; col++)
			{
				for (int row = 0; row < board.Size; row++)
				{
					if (board[row, col] != Marking && board[row, col] != '_')
						count++;

					if (board[row, col] == '_')
						blockingPos = row;

					if (count == board.Size - 1 && blockingPos != -1)
					{
						board.Mark(blockingPos, col, Marking);
						return;
					}
				}
				count = 0;
				blockingPos = -1;
			}

			_xoPlayer.MakeMove(board);
		}
	}
}
