﻿using System;

using Players;
using Boards;
using Factories;

namespace Decorators
{
	[RegisterInFactory(typeof(IDiagonalBlockerDecorator))]
	class XODiagonalBlockerDecorator:XOPlayerDecorator, IComputerPlayer,IDiagonalBlockerDecorator
	{
		public XODiagonalBlockerDecorator(IXOPlayer xoPlayer):base(xoPlayer)
		{

		}

		public override void MakeMove(IBoard board)
		{
			int count = 0, blockingRowPos = -1, blockingColPos = -1;

			for (int i = 0; i < board.Size; i++)
			{
				if (board[i, i] != Marking && board[i, i] != '_')
					count++;

				if (board[i, i] == '_')
					blockingRowPos = i;

				if (count == board.Size - 1 && blockingRowPos != -1)
				{
					board.Mark(blockingRowPos, blockingRowPos, Marking);
					return;
				}
			}

			count = 0;
			blockingRowPos = -1;

			for (int j = 0; j < board.Size; j++)
			{
				int i = board.Size - 1 - j;
                if (board[i, j] != Marking && board[i, j] != '_')
					count++;

				if (board[i, j] == '_')
				{
					blockingRowPos = i;
					blockingColPos = j;
                }

				if (count == board.Size - 1 && blockingRowPos != -1 && blockingColPos != -1)
				{
					board.Mark(blockingRowPos, blockingColPos, Marking);
					return;
				}
			}

			_xoPlayer.MakeMove(board);
		}
	}
}
