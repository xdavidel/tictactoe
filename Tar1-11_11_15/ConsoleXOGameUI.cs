﻿using Boards;
using Games;
using Players;
using System;
using System.Collections.Generic;

namespace Tar1_11_11_15
{
	class ConsoleXOGameUI
	{
		public ConsoleXOGameUI(XOGame game)
		{
			game.OnReadPlayer += ConsoleReadPlayer;
			game.OnReadDecoration += ConsoleReadDecoration;
			game.OnDisplayBoard += ConsoleDisplayBoard;
			game.OnUndoPlay += ConsoleUndoPlay;
			game.OnHumanRead += ReadHumanMove;
        }


		string ConsoleReadPlayer()
		{
			Console.Clear();
			Console.WriteLine("0 - Human Player");
			Console.WriteLine("1 - Computer Scan Player");
			Console.WriteLine("2 - Computer Random Player");
			Console.WriteLine("3 - Computer Random and Scan Player");
			Console.Write("Choose Player Kind: ");
			return Console.ReadLine();
		}

		List<string> ConsoleReadDecoration()
		{
			List<string> retVal = new List<string>();

			Console.Clear();
			Console.WriteLine("0 - Quit");
			Console.WriteLine("1 - Row Blocker Decoration");
			Console.WriteLine("2 - Column Blocker Decoration");
			Console.WriteLine("3 - Diagonal Blocker Decoration");
			Console.WriteLine("Choose Decoration Kind:");


			while (true)
			{
				string decoration = Console.ReadLine();
				if (decoration == "0")
					break;
				else
				{
					retVal.Add(decoration);
				}
			}

			return retVal;

		}

		void ConsoleDisplayBoard(IBoard board)
		{
			// =====Temp=============
			Console.WriteLine("Rows:");
			foreach (var item in (board as SquareBoard))
			{
				Console.Write(item + "  ");
			}
			Console.WriteLine();

			Console.WriteLine("Cols:");
			foreach (var item in (board as SquareBoard).ColumnScanner)
			{
				Console.Write(item + "  ");
			}
			Console.WriteLine();

			Console.WriteLine("DiagDown:");

			foreach (var item in (board as SquareBoard).DiagonalDownScanner)
			{
				Console.Write(item + "  ");
			}
			Console.WriteLine();

			Console.WriteLine("DiagUp:");

			foreach (var item in (board as SquareBoard).DiagonalUpScanner)
			{
				Console.Write(item + "  ");
			}
			Console.WriteLine();

			Console.WriteLine("Normal:");


			// =====Temp=============

			for (int i = 0; i < board.Size; i++)
			{
				for (int j = 0; j < board.Size; j++)
					Console.Write(board[i, j] + "\t");
				Console.WriteLine();
			}
			Console.WriteLine();
			
		}

		string ConsoleUndoPlay()
		{
			Console.WriteLine("Press 'z' to Undo");
			Console.WriteLine("Press 'r' to Redo");
			Console.WriteLine("Press any other key to continue: ");
			return Console.ReadLine();
		}

		void ReadHumanMove(HumanPlayer humanPlayer)
		{
			ConsoleMoveReader moveReader = new ConsoleMoveReader(humanPlayer);
		}
    }
}
