﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Factories;
using Games;
using Boards;

namespace Tar1_11_11_15
{
    class Program
    {
        static void Main(string[] args)
		{
			try
			{

				XOGame game = (IGame)Factory.Instance.CreateObject(typeof(IXOGame)) as XOGame;

				ConsoleXOGameUI xoUI = new ConsoleXOGameUI(game);

				game.SetupGame();

				while (true)
				{
					game.Run();
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}

        }
	}
}
