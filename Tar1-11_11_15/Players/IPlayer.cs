﻿using System;

using Boards;

namespace Players
{
    interface IPlayer
    {

		void MakeMove(IBoard board);
    }
}
