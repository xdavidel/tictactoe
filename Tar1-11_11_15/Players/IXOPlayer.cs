﻿using System;

using Boards;

namespace Players
{
	interface IXOPlayer : IPlayer
	{
		char Marking { get; set; }
	}
}
