﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Boards;
using Factories;

namespace Players
{
	[RegisterInFactory(typeof(IRandScanPlayer))]
	class RandScanPlayer : IXOPlayer, IRandScanPlayer, IComputerPlayer
	{
		IXOPlayer _randomPlayer;
		IXOPlayer _scanPlayer;

		protected char _marking;

		int _turn = 0;

		public char Marking
		{
			get
			{
				return _marking;
			}

			set
			{
				_scanPlayer.Marking = value;
				_randomPlayer.Marking = value;
				_marking = value;
			}
		}

		public RandScanPlayer()
		{
			Factory factory = Factory.Instance;
			_scanPlayer = factory.CreateObject(typeof(IScanPlayer)) as IXOPlayer;
			_randomPlayer = factory.CreateObject(typeof(IRandomPlayer)) as IXOPlayer;
		}

		public void MakeMove(IBoard board)
		{
			if (_turn++ == 0)
				_scanPlayer.MakeMove(board);
			else
			{
				_turn = 0;
				_randomPlayer.MakeMove(board);
			}
		}
	}
}
