﻿using System;

using Boards;
using Factories;

namespace Players
{
    [RegisterInFactory(typeof(IHumanPlayer))]
    class HumanPlayer:IXOPlayer,IHumanPlayer
    {
		protected string _name;
		protected char _marking;

		public event Func<IBoard, int[]> OnMoveRead;

		public char Marking
		{
			get
			{
				return _marking;
			}

			set
			{
				_marking = value;
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}

			set
			{
				_name = value;
			}
		}

		public void MakeMove(IBoard board)
		{
			if (OnMoveRead != null)
			{
				int[] pos = OnMoveRead(board);
				board.Mark(pos[0], pos[1], Marking);
			}
			else throw new NoReadMatrixCoordinatesException("No Registry to ReadMatrixCoordinates");
		}


		public class NoReadMatrixCoordinatesException:Exception
		{
			public NoReadMatrixCoordinatesException(string msg):base(msg)
			{

			}
		}
    }
}
