﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Boards;
using Factories;

namespace Players
{
    [RegisterInFactory(typeof(IScanPlayer))]
    class ScanPlayer:IXOPlayer,IScanPlayer,IComputerPlayer
    {
		protected char _marking;

		public char Marking
		{
			get
			{
				return _marking;
			}

			set
			{
				_marking = value;
			}
		}

		public void MakeMove(IBoard board)
		{
			int row = 0, col = 0;

			while (true)
			{
				if (col >= board.Size)
				{
					col = 0;
					row++;
				}

				if (board[row, col] == '_')
					break;
				else col++;

            }
			
			board.Mark(row,col,Marking);
		}
    }
}
