﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Boards;
using Factories;

namespace Players
{
    [RegisterInFactory(typeof(IRandomPlayer))]
    class RandomPlayer:IXOPlayer,IRandomPlayer, IComputerPlayer
	{
		protected char _marking;
		public char Marking
		{
			get
			{
				return _marking;
			}

			set
			{
				_marking = value;
			}
		}

		public void MakeMove(IBoard board)
		{
			Random r = new Random();
			int row;
			int col;

			while(true)
			{
				row = r.Next(board.Size);
				col = r.Next(board.Size);
                if (board[row, col] == '_')
					break;
			}

			board.Mark(row,col,Marking);

			
        }
    }
}
