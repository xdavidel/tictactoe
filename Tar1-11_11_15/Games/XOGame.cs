﻿using System;
using System.Collections.Generic;

using Players;
using Boards;
using Decorators;
using Factories;
using Tar1_11_11_15;

namespace Games
{

	[RegisterInFactory(typeof(IXOGame))]
	class XOGame : IXOGame
	{
		ISquareBoard _board;
		Queue<IXOPlayer> _players;
		
		Stack<IMemento> _undoMementos;
		Stack<IMemento> _redoMementos;

		Dictionary<string, Type> _playersOptions;
		Dictionary<string, Type> _decorationOptions;

		public event Func<string> OnReadPlayer;
		public event Func<List<string>> OnReadDecoration;
		public event Action<IBoard> OnDisplayBoard;
		public event Func<string> OnUndoPlay;
		public event Action<HumanPlayer> OnHumanRead;

		public XOGame()
		{
			_players = new Queue<IXOPlayer>();
			_undoMementos = new Stack<IMemento>();
			_redoMementos = new Stack<IMemento>();

			InitPlayersOptions();
			InitDecorationOptions();

		}


		public void Run()
		{
			if (!GameOver())
			{				

				while (_players.Peek() is IHuman)
				{
					string undo = OnUndoPlay();

					if (undo == "z" && _undoMementos.Count > 0)
					{
						_redoMementos.Push(CreateMemento());

						SetMemento(_undoMementos.Pop());
						OnDisplayBoard(_board);
					}
					else if(undo == "r" && _redoMementos.Count > 0)
                    {
						_undoMementos.Push(CreateMemento());

						SetMemento(_redoMementos.Pop());
						OnDisplayBoard(_board);
					}
					else
					{
						_undoMementos.Push(CreateMemento());
						break;
					}

				}

				IXOPlayer currentPlayer = _players.Dequeue();
				_players.Enqueue(currentPlayer);
				currentPlayer.MakeMove(_board);

				OnDisplayBoard(_board);

			}
			else
			{
				throw new Exception("Game Over!");
			}

		}

		public void SetupGame()
		{
			SetupBoard();
			SetupPlayers();

			OnDisplayBoard(_board);
		}

		private void InitPlayersOptions()
		{
			_playersOptions = new Dictionary<string, Type>();
			_playersOptions.Add("0", typeof(IHumanPlayer));
			_playersOptions.Add("1", typeof(IScanPlayer));
			_playersOptions.Add("2", typeof(IRandomPlayer));
			_playersOptions.Add("3", typeof(IRandScanPlayer));
		}

		private void InitDecorationOptions()
		{
			_decorationOptions = new Dictionary<string, Type>();
			_decorationOptions.Add("1", typeof(IRowBlockerPlayerDecorator));
			_decorationOptions.Add("2", typeof(IColumnBlockerDecorator));
			_decorationOptions.Add("3", typeof(IDiagonalBlockerDecorator));
		}

		private void SetupPlayers()
		{
			Factory factory = Factory.Instance;
			char[] marks = { 'X', 'O' };

			foreach (char mark in marks)
			{
				string uiOption = OnReadPlayer();

				if (!_playersOptions.ContainsKey(uiOption))
				{
					throw new OptionNotExistException("Invalid UI Option!", uiOption);
				}

				IXOPlayer curPlayer = factory.CreateObject(_playersOptions[uiOption]) as IXOPlayer;
				curPlayer.Marking = mark;

				// only for computer players
				if (curPlayer is IComputerPlayer)
				{
					if (OnReadDecoration != null)
					{
						List<string> decorationsToAdd;
						decorationsToAdd = OnReadDecoration();

						foreach (string decoration in decorationsToAdd)
						{
							factory.AddPlayerDecoration(_decorationOptions[decoration], ref curPlayer);
						}

					}
					
				}
				if (curPlayer is IHumanPlayer)
				{

					OnHumanRead(curPlayer as HumanPlayer);
				}

				_players.Enqueue(curPlayer);

			}

			
		}

		private void SetupBoard()
		{
			/*
			Console.Write("Enter board size: ");
			int boardSize = int.Parse(Console.ReadLine());
			
			int boardSize = 3;
			*/
			_board = Factory.Instance.CreateObject(typeof(ISquareBoard)) as SquareBoard;
		}

		private bool GameOver()
		{
			if (_board.EmptySpots == 0)
				return true;

			// Real Gameover check
			for (int i = 0; i < _board.Size; i++)
			{
				for (int j = 0; j < _board.Size; j++)
				{
					if (_board[i,j] != '_')
						if (CheckRow(i, j) || CheckCol(i, j) || CheckDiagonalDown(i, j) || CheckDiagonalUp(i, j))
							return true;
				}
			}

			return false;

		}

		private bool CheckRow(int row, int col, int count = 3)
		{
			if (col + count > _board.Size)
				return false;

			for (int i = col + 1; i < col + count; i++)
			{
				if (_board[row, col] != _board[row, i])
					return false;
			}

			return true;			
			
		}

		private bool CheckCol(int row, int col, int count = 3)
		{
			if (row + count > _board.Size)
				return false;

			for (int i = row + 1; i < row + count; i++)
			{
				if (_board[row, col] != _board[i, col])
					return false;
			}

			return true;

		}

		private bool CheckDiagonalDown(int row, int col, int count = 3)
		{
			if (row + count > _board.Size || col + count > _board.Size)
				return false;

			for (int i = row + 1; i < row + count; i++)
			{
				if (_board[row, col] != _board[i, i])
					return false;
			}

			return true;

		}

		private bool CheckDiagonalUp(int row, int col, int count = 3)
		{
			if (row - (count - 1) < 0 || col + count > _board.Size)
				return false;

			for (int i = row - 1, j = col +1; i > row - count; i--, j++)
			{
				if (_board[row, col] != _board[i, j])
					return false;
			}

			return true;

		}

		
		public class OptionNotExistException:Exception
		{
			public string UIOption { get; private set; }

			public OptionNotExistException(string msg,string uiOption):base(msg)
			{
				UIOption = uiOption;
			}
		}

		/// <summary>
		/// Save state
		/// </summary>
		/// <returns>Snapshot of current state</returns>
		public IMemento CreateMemento() { return new Memento(this); }

		/// <summary>
		/// Undo last play
		/// </summary>
		public void SetMemento(IMemento memento)
		{
			Memento gameMemento = (Memento)memento;

			SquareBoard sb = _board as SquareBoard;
			if (sb != null)
				sb.CopyFrom(gameMemento.Arr);

			_players.Clear();
			foreach (IXOPlayer player in gameMemento.Players)
			{
				_players.Enqueue(player);
			}

		}

		private class Memento:IMemento
		{
			char[,] _arr;
			IXOPlayer[] _players;

			public char[,] Arr
			{
				get
				{
					return _arr;
				}
			}

			public IXOPlayer[] Players
			{
				get
				{
					return _players;
				}
			}

			public Memento(XOGame game)
			{
				int size = game._board.Size;
                _arr = new char[size, size];

				SquareBoard sb = game._board as SquareBoard;
				if (sb != null)
					sb.CopyTo(ref _arr);

				_players = new IXOPlayer[game._players.Count];
				game._players.CopyTo(_players,0);
			}
		}
	}
}
